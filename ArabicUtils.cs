﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Globalization;
using UnityEngine;
using System.Linq;


public class ArabicChar
{
	/// <summary>
	/// The name of the character
	/// </summary>
	public string Name;

	/// <summary>
	/// The unicode value of the character
	/// </summary>
	public int UnicodeValue;

	/// <summary>
	/// The isolated form value of the character in the presentation forms B
	/// </summary>
	public int IsolatedFormValue;

	/// <summary>
	/// The leading form value of the character in the presentation forms B
	/// </summary>
	public int LeadingFormValue;

	/// <summary>
	/// The middle form value of the character in the presentation forms B
	/// </summary>
	public int MiddleFormValue;

	/// <summary>
	/// The trailing form value of the character in the presentation forms B
	/// </summary>
	public int TrailingFormValue;


	/// <summary>
	/// Wether this character forces the next character to take a leading form even if it's in the middle of a word
	/// </summary>
	public bool ForcesLeadingFormNext;



	public ArabicChar(string name, int unicodeValue, int isolatedFormValue, int leadingFormValue, int middleFormValue, int trailingFormValue, bool forcesLeadingFormNext) 
	{
		Name = name;
		UnicodeValue = unicodeValue;
		IsolatedFormValue = isolatedFormValue;
		LeadingFormValue = leadingFormValue;
		MiddleFormValue = middleFormValue;
		TrailingFormValue = trailingFormValue;
		ForcesLeadingFormNext = forcesLeadingFormNext;
	}

}



public class ArabicLigature
{
	/// <summary>
	/// The name of the ligature
	/// </summary>
	public string Name;

	/// <summary>
	/// The character sequence signature in isolated form value in Presentation Forms B
	/// </summary>
	public string Signature;

	/// <summary>
	/// The reversed character signature in isolated form value in Presentation Forms B
	/// The reversed signature is the one that is used for lookup since Unity flips all characters
	/// </summary>
	public string ReversedSignature;

	/// <summary>
	/// The isolated form value of the replacement character for this ligature in the presentation forms B
	/// </summary>
	public int ReplacementCharacter;



	public ArabicLigature(string name, char[] signatureSequence, int replacementCharacter)
	{
		Name = name;
		Signature = new string(signatureSequence);

		char[] reversedSequence = (char[])signatureSequence.Clone();
		Array.Reverse(reversedSequence);
		ReversedSignature = new string(reversedSequence);

		ReplacementCharacter = replacementCharacter;
	}

}



public static class ArabicUtils
{

	private static Dictionary<string, ArabicChar> ArabicCharacters = new Dictionary<string, ArabicChar>()
	{
		{"hamza",                                new ArabicChar("hamza",                                 0x0621, 0xfe80, 0xfe80, 0xfe80, 0xfe80, true)},
		{"alif_mad",                             new ArabicChar("alif mad",                              0x0622, 0xfe81, 0xfe81, 0xfe82, 0xfe82, true)},
		{"alif_hamza_faw9",                      new ArabicChar("alif hamza faw9",                       0x0623, 0xfe83, 0xfe83, 0xfe84, 0xfe84, true)},
		{"waw_hamza_faw9",                       new ArabicChar("waw hamza faw9",                        0x0624, 0xfe85, 0xfe86, 0xfe86, 0xfe86, true)},
		{"alif_hamza_ta7t",                      new ArabicChar("alif hamza ta7t",                       0x0625, 0xfe87, 0xfe87, 0xfe88, 0xfe88, true)},
		{"ya2_hamza_faw9",                       new ArabicChar("ya2 hamza faw9",                        0x0626, 0xfe89, 0xfe8b, 0xfe8c, 0xfe8a, false)},
		{"alif",                                 new ArabicChar("alif",                                  0x0627, 0xfe8d, 0xfe8d, 0xfe8e, 0xfe8e, true)},
		{"ba2",                                  new ArabicChar("ba2",                                   0x0628, 0xfe8f, 0xfe91, 0xfe92, 0xfe90, false)},
		{"ta2_marbouta",                         new ArabicChar("ta2 marbouta",                          0x0629, 0xfe93, 0xfe93, 0xfe94, 0xfe94, true)},
		{"ta2",                                  new ArabicChar("ta2",                                   0x062a, 0xfe95, 0xfe97, 0xfe98, 0xfe96, false)},
		{"tha2",                                 new ArabicChar("tha2",                                  0x062b, 0xfe99, 0xfe9b, 0xfe9c, 0xfe9a, false)},
		{"jim",                                  new ArabicChar("jim",                                   0x062c, 0xfe9d, 0xfe9f, 0xfea0, 0xfe9e, false)},
		{"7a2",                                  new ArabicChar("7a2",                                   0x062d, 0xfea1, 0xfea3, 0xfea4, 0xfea2, false)},
		{"kha2",                                 new ArabicChar("kha2",                                  0x062e, 0xfea5, 0xfea7, 0xfea8, 0xfea6, false)},
		{"dal",                                  new ArabicChar("dal",                                   0x062f, 0xfea9, 0xfea9, 0xfeaa, 0xfeaa, true)},
		{"dhal",                                 new ArabicChar("dhal",                                  0x0630, 0xfeab, 0xfeab, 0xfeac, 0xfeac, true)},
		{"ra2",                                  new ArabicChar("ra2",                                   0x0631, 0xfead, 0xfead, 0xfeae, 0xfeae, true)},
		{"zay",                                  new ArabicChar("zay",                                   0x0632, 0xfeaf, 0xfeb0, 0xfeb0, 0xfeb0, true)},
		{"cin",                                  new ArabicChar("cin",                                   0x0633, 0xfeb1, 0xfeb3, 0xfeb4, 0xfeb2, false)},
		{"chin",                                 new ArabicChar("chin",                                  0x0634, 0xfeb5, 0xfeb7, 0xfeb8, 0xfeb6, false)},
		{"ssad",                                 new ArabicChar("ssad",                                  0x0635, 0xfeb9, 0xfebb, 0xfebc, 0xfeba, false)},
		{"ddad",                                 new ArabicChar("ddad",                                  0x0636, 0xfebd, 0xfebf, 0xfec0, 0xfebe, false)},
		{"tta2",                                 new ArabicChar("tta2",                                  0x0637, 0xfec1, 0xfec3, 0xfec4, 0xfec2, false)},
		{"dhad",                                 new ArabicChar("dhad",                                  0x0638, 0xfec5, 0xfec7, 0xfec8, 0xfec6, false)},
		{"3ayn",                                 new ArabicChar("3ayn",                                  0x0639, 0xfec9, 0xfecb, 0xfecc, 0xfeca, false)},
		{"ghayn",                                new ArabicChar("ghayn",                                 0x063a, 0xfecd, 0xfecf, 0xfed0, 0xfece, false)},
		{"fa2",                                  new ArabicChar("fa2",                                   0x0641, 0xfed1, 0xfed3, 0xfed4, 0xfed2, false)},
		{"9af",                                  new ArabicChar("9af",                                   0x0642, 0xfed5, 0xfed7, 0xfed8, 0xfed6, false)},
		{"kaf",                                  new ArabicChar("kaf",                                   0x0643, 0xfed9, 0xfedb, 0xfedc, 0xfeda, false)},
		{"lam",                                  new ArabicChar("lam",                                   0x0644, 0xfedd, 0xfedf, 0xfee0, 0xfede, false)},
		{"mim",                                  new ArabicChar("mim",                                   0x0645, 0xfee1, 0xfee3, 0xfee4, 0xfee2, false)},
		{"noun",                                 new ArabicChar("noun",                                  0x0646, 0xfee5, 0xfee7, 0xfee8, 0xfee6, false)},
		{"ha2",                                  new ArabicChar("ha2",                                   0x0647, 0xfee9, 0xfeeb, 0xfeec, 0xfeea, false)},
		{"waw",                                  new ArabicChar("waw",                                   0x0648, 0xfeed, 0xfeed, 0xfeee, 0xfeee, true)},
		{"alif_ma9sora",                         new ArabicChar("alif ma9sora",                          0x0649, 0xfeef, 0xfef0, 0xfef0, 0xfef0, true)},
		{"ya2",                                  new ArabicChar("ya2",                                   0x064a, 0xfef1, 0xfef3, 0xfef4, 0xfef2, false)},
		{"alif_wasl",                            new ArabicChar("alif wasl",                             0x0671, 0xfe8d, 0xfe8d, 0xfe8d, 0xfe8d, true)},
		{"lig_lam_alif_mam",                     new ArabicChar("lig: lam + alif mamdoudah",             0x0000, 0xfef5, 0xfef5, 0xfef6, 0xfef6, true)},
		{"lig_lam_alif_hamza_faw9",              new ArabicChar("lig: lam + alif hamza faw9",            0x0000, 0xfef7, 0xfef7, 0xfef8, 0xfef8, true)},
		{"lig_lam_alif_hamza_ta7t",              new ArabicChar("lig: lam + alif hamza ta7t",            0x0000, 0xfef9, 0xfef9, 0xfefa, 0xfefa, true)},
		{"lig_lam_alif",                         new ArabicChar("lig: lam + alif",                       0x0000, 0xfefb, 0xfefb, 0xfefc, 0xfefc, true)}
	};


	private static Dictionary<int, ArabicChar> ArabicCharactersByUnicode = new Dictionary<int, ArabicChar>()
	{
		{0x0621, ArabicCharacters["hamza"]},
		{0x0622, ArabicCharacters["alif_mad"]},
		{0x0623, ArabicCharacters["alif_hamza_faw9"]},
		{0x0624, ArabicCharacters["waw_hamza_faw9"]},
		{0x0625, ArabicCharacters["alif_hamza_ta7t"]},
		{0x0626, ArabicCharacters["ya2_hamza_faw9"]},
		{0x0627, ArabicCharacters["alif"]},
		{0x0628, ArabicCharacters["ba2"]},
		{0x0629, ArabicCharacters["ta2_marbouta"]},
		{0x062a, ArabicCharacters["ta2"]},
		{0x062b, ArabicCharacters["tha2"]},
		{0x062c, ArabicCharacters["jim"]},
		{0x062d, ArabicCharacters["7a2"]},
		{0x062e, ArabicCharacters["kha2"]},
		{0x062f, ArabicCharacters["dal"]},
		{0x0630, ArabicCharacters["dhal"]},
		{0x0631, ArabicCharacters["ra2"]},
		{0x0632, ArabicCharacters["zay"]},
		{0x0633, ArabicCharacters["cin"]},
		{0x0634, ArabicCharacters["chin"]},
		{0x0635, ArabicCharacters["ssad"]},
		{0x0636, ArabicCharacters["ddad"]},
		{0x0637, ArabicCharacters["tta2"]},
		{0x0638, ArabicCharacters["dhad"]},
		{0x0639, ArabicCharacters["3ayn"]},
		{0x063a, ArabicCharacters["ghayn"]},
		{0x0641, ArabicCharacters["fa2"]},
		{0x0642, ArabicCharacters["9af"]},
		{0x0643, ArabicCharacters["kaf"]},
		{0x0644, ArabicCharacters["lam"]},
		{0x0645, ArabicCharacters["mim"]},
		{0x0646, ArabicCharacters["noun"]},
		{0x0647, ArabicCharacters["ha2"]},
		{0x0648, ArabicCharacters["waw"]},
		{0x0649, ArabicCharacters["alif_ma9sora"]},
		{0x064a, ArabicCharacters["ya2"]},
		{0x0672, ArabicCharacters["alif_hamza_faw9"]},
		{0x0673, ArabicCharacters["alif_hamza_ta7t"]},
		{0x0671, ArabicCharacters["alif_wasl"]}
	};


	private static Dictionary<int, ArabicChar> ArabicCharactersByIsolatedForm = new Dictionary<int, ArabicChar>()
	{
		{0xfe80, ArabicCharacters["hamza"]},
		{0xfe81, ArabicCharacters["alif_mad"]},
		{0xfe83, ArabicCharacters["alif_hamza_faw9"]},
		{0xfe85, ArabicCharacters["waw_hamza_faw9"]},
		{0xfe87, ArabicCharacters["alif_hamza_ta7t"]},
		{0xfe89, ArabicCharacters["ya2_hamza_faw9"]},
		{0xfe8d, ArabicCharacters["alif"]},
		{0xfe8f, ArabicCharacters["ba2"]},
		{0xfe93, ArabicCharacters["ta2_marbouta"]},
		{0xfe95, ArabicCharacters["ta2"]},
		{0xfe99, ArabicCharacters["tha2"]},
		{0xfe9d, ArabicCharacters["jim"]},
		{0xfea1, ArabicCharacters["7a2"]},
		{0xfea5, ArabicCharacters["kha2"]},
		{0xfea9, ArabicCharacters["dal"]},
		{0xfeab, ArabicCharacters["dhal"]},
		{0xfead, ArabicCharacters["ra2"]},
		{0xfeaf, ArabicCharacters["zay"]},
		{0xfeb1, ArabicCharacters["cin"]},
		{0xfeb5, ArabicCharacters["chin"]},
		{0xfeb9, ArabicCharacters["ssad"]},
		{0xfebd, ArabicCharacters["ddad"]},
		{0xfec1, ArabicCharacters["tta2"]},
		{0xfec5, ArabicCharacters["dhad"]},
		{0xfec9, ArabicCharacters["3ayn"]},
		{0xfecd, ArabicCharacters["ghayn"]},
		{0xfed1, ArabicCharacters["fa2"]},
		{0xfed5, ArabicCharacters["9af"]},
		{0xfed9, ArabicCharacters["kaf"]},
		{0xfedd, ArabicCharacters["lam"]},
		{0xfee1, ArabicCharacters["mim"]},
		{0xfee5, ArabicCharacters["noun"]},
		{0xfee9, ArabicCharacters["ha2"]},
		{0xfeed, ArabicCharacters["waw"]},
		{0xfeef, ArabicCharacters["alif_ma9sora"]},
		{0xfef1, ArabicCharacters["ya2"]},
		{0xfef5, ArabicCharacters["lig_lam_alif_mam"]},
		{0xfef7, ArabicCharacters["lig_lam_alif_hamza_faw9"]},
		{0xfef9, ArabicCharacters["lig_lam_alif_hamza_ta7t"]},
		{0xfefb, ArabicCharacters["lig_lam_alif"]}
	};


	private static List<ArabicLigature> ArabicLigatures = new List<ArabicLigature>()
	{
		new ArabicLigature("lam + alif mamdoudah",       new char[] { '\uFEDD', '\uFE81' }, 0xfef5),
		new ArabicLigature("lam + alif hamza faw9",      new char[] { '\uFEDD', '\uFE83' }, 0xfef7),
		new ArabicLigature("lam + alif hamza ta7t",      new char[] { '\uFEDD', '\uFE87' }, 0xfef9),
		new ArabicLigature("lam + alif",                 new char[] { '\uFEDD', '\uFE8d' }, 0xfefb)
	};





	/// <summary>
	/// Processes a heterogeneous text that contains Arabic characters as well as other characters
	/// </summary>
	public static string ProcessText(string text)
	{

		if (String.IsNullOrEmpty(text))
		{
			return String.Empty;
		}


		string processedString = String.Empty;


		// Normalize new line endings so they become platform independant
		text = Regex.Replace(text, @"\r\n|\n\r|\n|\r|\\n", Environment.NewLine);


		// Split the input string into lines
		string[] separator = new string[] { Environment.NewLine };
		string[] lines = text.Split(separator, StringSplitOptions.RemoveEmptyEntries);

		for (int i = 0; i < lines.Length; i++)
		{
			processedString += ProcessLine(lines[i]);
			if (i < lines.Length - 1)
			{
				processedString += Environment.NewLine;
			}
		}

		return processedString;

	}


	/// <summary>
	/// Processes a heterogeneous line of text that contains Arabic characters
	/// </summary>
	/// <returns>The line.</returns>
	private static string ProcessLine(string line)
	{

		List<string> words = line.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries).ToList();

		for (int i = 0; i < words.Count; i++)
		{
			words[i] = ProcessWord(words[i]);
		}

		words.Reverse();
		return string.Join(" ", words.ToArray());

	}


	/// <summary>
	/// Processes the Arabic string
	/// </summary>
	private static string ProcessWord(string word)
	{

		List<string> wordParts = new List<string>();


		// Extract all arabic letters from the word
		bool foundArabicSubWord = false;
		int arabicSubWordStartIndex = -1;
		int arabicSubWordEndIndex = 0;

		for (int i = 0; i < word.Length; i++)
		{
			int character = (int)word[i];
			if (ArabicCharactersByUnicode.ContainsKey(character))
			{
				if (!foundArabicSubWord)
				{
					foundArabicSubWord = true;
					arabicSubWordStartIndex = i;

					if (arabicSubWordEndIndex < arabicSubWordStartIndex)
					{
						wordParts.Add(word.Substring(arabicSubWordEndIndex, arabicSubWordStartIndex - arabicSubWordEndIndex));
					}
				}
				else
				{
					if (i == word.Length - 1)
					{
						foundArabicSubWord = false;
						arabicSubWordEndIndex = i;

						string arabicWord = word.Substring(arabicSubWordStartIndex);
						string processedArabicWord = ProcessArabicWord(arabicWord);

						wordParts.Add(processedArabicWord);
					}
				}

			}
			else
			{
				if (foundArabicSubWord)
				{
					foundArabicSubWord = false;
					arabicSubWordEndIndex = i;

					string arabicWord = word.Substring(arabicSubWordStartIndex, arabicSubWordEndIndex - arabicSubWordStartIndex);
					string processedArabicWord = ProcessArabicWord(arabicWord);

					wordParts.Add(processedArabicWord);
				}
			}
		}


		if (arabicSubWordEndIndex < word.Length - 1)
		{
			string lastPart = word.Substring (arabicSubWordEndIndex);
			wordParts.Add(lastPart);
		}


		wordParts.Reverse();
		return string.Join("", wordParts.ToArray());

	}


	/// <summary>
	/// Processes a pure arabic word that contains supported arabic letter only
	/// </summary>
	/// <returns>The processed arabic word.</returns>
	/// <param name="arabicWord">Arabic word with supported arabic letter only, no punctuation, no garbage</param>
	private static string ProcessArabicWord(string arabicWord)
	{

		if (String.IsNullOrEmpty(arabicWord))
		{
			return String.Empty;
		}


		// Reverse all arabic characters inside the word to counter Unity's own reversal
		string processedArabicWord = Reverse(arabicWord);
		char[] characters = processedArabicWord.ToCharArray();


		// Convert all arabic characters to Presentation Forms B isolated form
		for (int i = 0; i < characters.Length; i++)
		{
			int character = (int)characters[i];
			if (ArabicCharactersByUnicode.ContainsKey(character))
			{
				characters[i] = (char)ArabicCharactersByUnicode[character].IsolatedFormValue;
			}
		}

		processedArabicWord = new string(characters);


		// Perform ligature lookup
		for (int i = 0; i < ArabicLigatures.Count; i++)
		{
			processedArabicWord = processedArabicWord.Replace(ArabicLigatures[i].ReversedSignature, Char.ConvertFromUtf32(ArabicLigatures[i].ReplacementCharacter));
		}


		// Replace characaters with their position-aware version
		characters = processedArabicWord.ToCharArray();

		for (int i = 0; i < characters.Length; i++)
		{
			int character = (int)characters[i];
			if (ArabicCharactersByIsolatedForm.ContainsKey(character))
			{
				
				if (i == 0) // The last character in the word since the string is reversed
				{
					if (characters.Length > 1)
					{
						int nextCharacter = (int)characters[i + 1];
						if (ArabicCharactersByIsolatedForm[nextCharacter].ForcesLeadingFormNext)
						{
							characters[i] = (char)ArabicCharactersByIsolatedForm[character].IsolatedFormValue;
						}
						else
						{
							characters[i] = (char)ArabicCharactersByIsolatedForm[character].TrailingFormValue;
						}
					}
					else
					{
						characters[i] = (char)ArabicCharactersByIsolatedForm[character].IsolatedFormValue;
					}

				}

				else if (i == characters.Length - 1) // The first character in the word since the string is reversed
				{
					
					characters[i] = (char)ArabicCharactersByIsolatedForm[character].LeadingFormValue;

				}

				else // A character in the middle of the word
				{

					// If next char has a middle form that is similar to its trailing form then it forces a leading char before it
					int nextCharacter = (int)characters[i + 1];
					if (ArabicCharactersByIsolatedForm[nextCharacter].ForcesLeadingFormNext) 
					{
						characters[i] = (char)ArabicCharactersByIsolatedForm[character].LeadingFormValue;
					}
					else
					{
						characters[i] = (char)ArabicCharactersByIsolatedForm[character].MiddleFormValue;
					}

				}
			}
		}


		processedArabicWord = new string(characters);
		return processedArabicWord;

	}


	/// <summary>
	/// Reverse the order of characters in a string.
	/// </summary>
	public static string Reverse(string s)
	{
		char[] charArray = s.ToCharArray();
		Array.Reverse( charArray );
		return new string( charArray );
	}

}


