# Welcome #

ArabicUtils is a support library that fixes Arabic text rendering for Unity until official support for RTL languages becomes available.


### How do I get set up? ###

1. Add ArabicUtils.cs to your Unity project
2. Create Arabic text in any editor you like, I personally use [Arabic Keyboard ](http://www.arabic-keyboard.org/)
3. Make the text available to your code either from a file or directly from a string
4. Call ArabicUtils.ProcessArabicText with your Arabic text as parameter
5. If you are having trouble, check out the test project in the [Downloads](https://bitbucket.org/younesouhbi/arabic-support-for-unity/downloads) page

### Contribution guidelines ###

I am creating this library to be used for our own game so it's far from complete but I'll be updating it as I go along. Feel free to post any issue you find in the [issue tracker](https://bitbucket.org/younesouhbi/arabic-support-for-unity/issues?status=new&status=open) and do not hesitate to contribute with any enhancements by creating a pull request.